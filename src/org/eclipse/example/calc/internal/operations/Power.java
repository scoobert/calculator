package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.UnaryOperation;

/* Power operation */
public class Power extends AbstractOperation implements UnaryOperation {
	
	
	@Override
	public float perform(float arg1) {
		return arg1 * arg1;
	}

	@Override
	public String getName() {
		return "Power";
	}
}